"""empty message

Revision ID: 7b02b21d7516
Revises: 92a627f38c87
Create Date: 2020-10-04 23:15:31.553623

"""
from alembic import op
import sqlalchemy as sa



# revision identifiers, used by Alembic.
revision = '7b02b21d7516'
down_revision = '92a627f38c87'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # ### commands auto generated by Alembic - please adjust! ###
    # op.add_column('menu', sa.Column('menu_pai_codigo', sa.Integer(), nullable=True))
    # op.create_foreign_key(None, 'menu', 'menu', ['menu_pai_codigo'], ['codigo'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'menu', type_='foreignkey')
    op.drop_column('menu', 'menu_pai_codigo')
    # ### end Alembic commands ###
