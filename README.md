# Projeto de Teste - Kabum

Projeto de teste de admissão - Kabum

Executar:

```
python run.py runserver
```

API de produtos:

GET localhost:5000/products/description/<id>

Retorno exemplo:
```
{
  "avaliacao_nota": 5,
  "avaliacao_numero": 1239,
  "brinde": null,
  "codigo": 85198,
  "codigo_anatel": "",
  "desconto": 15,
  "descricao": "Transforme seu PC em um foguete com o SSD A400 da Kingston. Performance incrível com uma controladora de última geração para velocidades de leitura e gravação de até 500MB/s e 450MB/s. Este SSD é 10x mais rápido do que um disco rígido tradicional para melhor desempenho, resposta ultrarrápida para um computador mais rápido de modo geral. O seu desktop merece esse grande upgrade de velocidade!\\n\\nEsta unidade de estado sólido, comparados a discos rígidos mecânicos, aumenta de forma drástica a resposta do seu PC com tempos maravilhosos de inicialização, carregamento e transferência. \\nEste SSD também é mais confiável e durável do que um disco rígido comum, com toda a segurança que a Kingston pode te proporcionar com 480GB de capacidade. Para que você não se preocupe com espaço e foque no jogo. O A400 também conta com a tecnologia 3D NAND (também chamada de V-NAND). Procurando SSD? Compre no KaBuM!",
  "dimensao_peso": 70,
  "disponibilidade": true,
  "economize_prime": 467.9,
  "fabricante": {
    "codigo": 50,
    "img": "https://images0.kabum.com.br/produtos/fabricantes/logo-kingston.jpg",
    "nome": "Kingston"
  },
  "familia": {
    "codigo": 0,
    "nome": "Kingston",
    "produtos": [
      {
        "caminho": "produtos/fotos/85197",
        "codigo": 85197,
        "disponibilidade": true,
        "foto": "https://images7.kabum.com.br/produtos/fotos/85197/85197_index_m.jpg",
        "link_descricao": "/produto/85197/ssd-kingston-a400-240gb-sata-leitura-500mb-s-grava-o-350mb-s-sa400s37-240g",
        "nome": "SSD Kingston A400, 240GB, SATA, Leitura 500MB/s, Gravação 350MB/s - SA400S37/240G",
        "preco": 317.53,
        "preco_antigo": 446.94,
        "preco_desconto": 269.9,
        "preco_desconto_prime": 0.0,
        "preco_prime": 0.0,
        "produto_prime": false
      }
    ],
    "titulo": "Fabricante"
  },
  "flag_blackfriday": 0,
  "fotos": [
    "https://images8.kabum.com.br/produtos/fotos/85198/85198_index_{}.jpg",
    "https://images8.kabum.com.br/produtos/fotos/85198/85198_1484306114_{}.jpg",
    "https://images8.kabum.com.br/produtos/fotos/85198/85198_1484306119_{}.jpg"
  ],
  "frete_gratis_somente_prime": false,
  "garantia": "1 ano de garantia",
  "id_oferta": "",
  "is_openbox": false,
  "link_descricao": "/produto/85198/ssd-kingston-a400-480gb-sata-leitura-500mb-s-grava-o-450mb-s-sa400s37-480g",
  "menu": "Harware/SATA/SSD/480.0 GB",
  "menus": [
    {
      "amigavel": "/hardware/ssd-2-5/ssd-2-5/480-0-gb",
      "codigo": 1660,
      "nome": "480.0 GB",
      "nome_url": "/Harware/SATA/SSD/480.0 GB"
    },
    {
      "amigavel": "/hardware/ssd-2-5/ssd-2-5",
      "codigo": 1432,
      "nome": "SSD",
      "nome_url": "/Harware/SATA/SSD"
    },
    {
      "amigavel": "/hardware/ssd-2-5",
      "codigo": 105,
      "nome": "SATA",
      "nome_url": "/Harware/SATA"
    },
    {
      "amigavel": "/hardware",
      "codigo": 1,
      "nome": "Hardware",
      "nome_url": "/Harware"
    }
  ],
  "nome": "SSD Kingston A400, 480GB, SATA, Leitura 500MB/s, Gravação 450MB/s - SA400S37/480G",
  "nova_descricao": "index.html",
  "oferta": {
    "codigo": "2f77e809c9034ff9920495f02e661af533ec1573",
    "data_fim": 1602586800,
    "data_inicio": 1601389620,
    "evento": "Hora do Play",
    "evento_codigo": 129,
    "logar": 0,
    "quantidade": 860
  },
  "oferta_inicio": 0,
  "origem": null,
  "origem_nome": null,
  "peso": "70 gramas (bruto com embalagem)",
  "pre_venda": false,
  "preco": 550.47,
  "preco_antigo": 623.41,
  "preco_desconto": 467.9,
  "preco_desconto_prime": 0.0,
  "preco_prime": 0.0,
  "produto_especie": 0,
  "produto_html": "<p><strong>Caracter&iacute;sticas:</strong></p><p>- Marca: Kingston</p><p>- Modelo: SA400S37/480G</p><p>&nbsp;</p><p><strong>Especifica&ccedil;&otilde;es:</strong></p><p>- Formato: 2,5 pol&nbsp;</p><p>- Interface: SATA Rev. 3.0 (6Gb/s) &mdash; compat&iacute;vel com a vers&atilde;o anterior SATA Rev. 2.0 (3Gb/s)</p><p>- Capacidades: 480GB</p><p>- NAND: TLC&nbsp;</p><p>- Performance de refer&ecirc;ncia - at&eacute; 500MB/s para leitura e 450MB/s para grava&ccedil;&atilde;o</p><p>- Temperatura de armazenamento: -40 &deg;C a 85 &deg;C&nbsp;</p><p>- Temperatura de opera&ccedil;&atilde;o: 0 &deg;C a 70 &deg;C</p><p>- Vibra&ccedil;&atilde;o quando em opera&ccedil;&atilde;o: 2,17G pico (7 &ndash; 800 Hz)</p><p>- Vibra&ccedil;&atilde;o quando n&atilde;o est&aacute; em opera&ccedil;&atilde;o: 20G pico (10 &ndash; 2000 Hz)</p><p>- Expectativa de vida &uacute;til: 1 milh&atilde;o de horas MTB</p><p>&nbsp;</p><p><strong>Benef&iacute;cios:&nbsp;</strong></p><p>- 10x mais r&aacute;pido do que um disco r&iacute;gido: Com incr&iacute;veis velocidades de leitura/grava&ccedil;&atilde;o, o SSD A400 n&atilde;o somente ir&aacute; aumentar o desempenho, como tamb&eacute;m poder&aacute; ser usado para dar vida nova em computadores mais antigos.&nbsp;</p><p>- Robusto: O A400 &eacute; resistente a impactos e vibra&ccedil;&otilde;es, para confiabilidade refor&ccedil;ada em notebooks e outros dispositivos m&oacute;veis.&nbsp;</p><p>- Ideal para desktops e notebooks: A400 tem um formato de 7 mm parase ajustar auma grande variedade de computadores. &Eacute; ideal para notebooks mais finos e computadores, ultrabooks e ultratop com espa&ccedil;o limitado.</p><p><strong><br /></strong></p><p><strong>Conte&uacute;do da embalagem:</strong></p><p>- SSD Kingston</p>",
  "sucesso": true,
  "tag_description": "Encontre SSD Kingston A400 no KaBuM! Armazenamento de 480GB, Cabo SATA, Rápida Inicialização, Carregamento e Transferência de Arquivos! Aproveite",
  "tag_title": "SSD Kingston 2.5´ 480GB A400 | KaBuM!",
  "tem_frete_gratis": false,
  "vendedor_nome": ""
}
```

## Informações:

- Banco de dados mockado com único produto, ID 85198 e ID 85197 (Para estrutura de família).
- Implementada autenticação com JWT, não utilizada na API implementada.