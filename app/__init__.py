from flask import Flask
from flask_jwt import JWT
from flask_migrate import Migrate
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy

from app.auth.controllers import authenticate, identity


app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
migrate = Migrate(app, db, render_as_batch=True)
jwt = JWT(app, authenticate, identity)

from app.modules import MODULES
for module in MODULES:
    app.register_blueprint(module)

