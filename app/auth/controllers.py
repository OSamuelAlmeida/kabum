from werkzeug.security import safe_str_cmp
from app.auth.models import User

users = [
    User(1, 'admin', 'admin')
]

username_table = {user.username: user for user in users}
user_id_table = {user.id: user for user in users}

def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user

def identity(payload):
    user_id = payload['identity']
    return user_id_table.get(user_id, None)

