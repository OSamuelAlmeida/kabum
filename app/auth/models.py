from dataclasses import dataclass, field

@dataclass
class User:
    id: int
    username: str
    password: str = field(repr=False)

