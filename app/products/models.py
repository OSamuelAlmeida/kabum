from app import db

class Evento(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(120))
    ofertas = db.relationship('Oferta', backref='evento', lazy=True)

    
class Oferta(db.Model):
    codigo = db.Column(db.String(40), primary_key=True)
    evento_codigo = db.Column(db.Integer, db.ForeignKey('evento.codigo'), nullable=False)
    quantidade = db.Column(db.Integer, nullable=False)
    data_inicio = db.Column(db.Integer, nullable=False)
    data_fim = db.Column(db.Integer, nullable=False)
    logar = db.Column(db.Boolean, nullable=False)
    produtos = db.relationship('Produto', backref='oferta', lazy=True)
    

class Vendedor(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(180), nullable=False)
    produtos = db.relationship('Produto', backref='vendedor', lazy=True)


class Menu(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(120), nullable=False)
    amigavel = db.Column(db.String(120), nullable=False)
    nome_url = db.Column(db.String(120), nullable=False)
    menu_pai_codigo = db.Column(db.Integer, db.ForeignKey('menu.codigo'), nullable=True)
    # menus_filhos = db.relationship('Menu', remote_side=codigo, backref='menus_filhos')
    produtos = db.relationship('Produto', backref='menu', lazy=True)

    def get_amigavel(self):
        menu_pai = Menu.query.get(self.menu_pai_codigo)
        inicio = menu_pai.get_amigavel() if menu_pai else ''
            
        return inicio + self.amigavel

    def get_nome_url(self):
        menu_pai = Menu.query.get(self.menu_pai_codigo)
        inicio = menu_pai.get_nome_url() if menu_pai else ''
            
        return inicio + self.nome_url

produtos_familia = db.Table('produtos',
    db.Column('produto_codigo', db.Integer, db.ForeignKey('produto.codigo'), primary_key=True),
    db.Column('familia_codigo', db.Integer, db.ForeignKey('familia.codigo'), primary_key=True)
)
    
class Familia(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(120), nullable=False)
    titulo = db.Column(db.String(120), nullable=False)
    produtos = db.relationship('Produto', secondary=produtos_familia, lazy='subquery',
        backref=db.backref('familias', lazy=True))
    

class Fabricante(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(120), nullable=False)
    img = db.Column(db.String(255), nullable=False)
    produtos = db.relationship('Produto', backref='fabricante', lazy=True)

    
class Foto(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(255), nullable=False)
    principal = db.Column(db.Boolean, nullable=False, default=False)
    produto_codigo = db.Column(db.Integer, db.ForeignKey('produto.codigo'), nullable=False)
    
    def get_size(self, size):
        url = self.url.format(size)

        return url


class Origem(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(120), nullable=False)
    produtos = db.relationship('Produto', backref='origem', lazy=True)

    
class Produto(db.Model):
    codigo = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(255), nullable=False)
    link_descricao = db.Column(db.String(255), nullable=False)
    brinde = db.Column(db.Integer, db.ForeignKey('Produto.codigo'), nullable=True)
    oferta_codigo = db.Column(db.String(40), db.ForeignKey('oferta.codigo'), nullable=True)
    vendedor_codigo = db.Column(db.Integer, db.ForeignKey('vendedor.codigo'), nullable=True)
    nova_descricao = db.Column(db.String(255), nullable=True)
    menu_codigo = db.Column(db.Integer, db.ForeignKey('menu.codigo'), nullable=False)
    fotos = db.relationship('Foto', backref='produto')
    brinde = db.relationship('Menu', backref='menus_filhos')
    disponibilidade = db.Column(db.Boolean, nullable=False)
    prevenda = db.Column(db.Boolean, nullable=False)
    fabricante_codigo = db.Column(db.Integer, db.ForeignKey('fabricante.codigo'), nullable=False)
    produto_prime = db.Column(db.Boolean, nullable=False)
    preco = db.Column(db.Float, nullable=False)
    preco_prime = db.Column(db.Float, nullable=False, default=0.0)
    preco_desconto = db.Column(db.Float, nullable=False)
    preco_desconto_prime = db.Column(db.Float, nullable=False, default=0.0)
    preco_antigo = db.Column(db.Float, nullable=False)
    economize_prime = db.Column(db.Float, nullable=False)
    descricao = db.Column(db.Text, nullable=False)
    tag_title = db.Column(db.String(255), nullable=False)
    tem_frete_gratis = db.Column(db.Boolean, nullable=False)
    frete_gratis_somente_prime = db.Column(db.Boolean, nullable=False)
    tag_description = db.Column(db.Text, nullable=False)
    avaliacao_numero = db.Column(db.Integer, nullable=False)
    avaliacao_nota = db.Column(db.Integer, nullable=False)
    desconto = db.Column(db.Integer, nullable=False, default=0)
    is_openbox = db.Column(db.Boolean, nullable=False)
    produto_html = db.Column(db.Text, nullable=False)
    dimensao_peso = db.Column(db.Integer, nullable=False)
    peso = db.Column(db.String(255), nullable=False)
    garantia = db.Column(db.String(255), nullable=False)
    codigo_anatel = db.Column(db.String(255), nullable=True)
    produto_especie = db.Column(db.Integer, nullable=False)
    link_descricao = db.Column(db.String(255), nullable=False)
    origem_codigo = db.Column(db.Integer, db.ForeignKey('origem.codigo'), nullable=True)
    flag_blackfriday = db.Column(db.Integer, nullable=False)
    sucesso = db.Column(db.Boolean, nullable=False)
    
