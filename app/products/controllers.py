from flask import Blueprint, jsonify
from flask_restful import Api, Resource

from app.products.models import Familia, Menu, Produto

products_module = Blueprint('products', __name__, url_prefix='/products')
api = Api(products_module)


class ProductDescriptionAPI(Resource):
    def get(self, id):
        produto = Produto.query.get(id)

        if oferta := produto.oferta:
            oferta = {
                'codigo': oferta.codigo,
                'evento_codigo': oferta.evento_codigo,
                'data_inicio': oferta.data_inicio,
                'data_fim': oferta.data_fim,
                'quantidade': oferta.quantidade,
                'evento': oferta.evento.nome,
                'logar': int(oferta.logar)
            }

        vendedor_nome = produto.vendedor.nome if produto.vendedor else ''

        menu = produto.menu
        menu_nome = menu.get_nome_url()[1:]
        menus = []
        menus.append({
            'codigo': menu.codigo,
            'nome': menu.nome,
            'amigavel': menu.get_amigavel(),
            'nome_url': menu.get_nome_url()
        })

        while menu_pai_codigo := menu.menu_pai_codigo:
            menu = Menu.query.get(menu_pai_codigo)
            menus.append({
                'codigo': menu.codigo,
                'nome': menu.nome,
                'amigavel': menu.get_amigavel(),
                'nome_url': menu.get_nome_url()
            })

        familia = {
            'codigo': produto.familias[0].codigo,
            'nome': produto.familias[0].nome,
            'titulo': produto.familias[0].titulo,
            'produtos': [
                {
                    'codigo': product.codigo,
                    'nome': product.nome,
                    'preco': product.preco,
                    'preco_prime': product.preco_prime,
                    'preco_antigo': product.preco_antigo,
                    'disponibilidade': product.disponibilidade,
                    'caminho': 'produtos/fotos/{}'.format(product.codigo),
                    'preco_desconto': product.preco_desconto,
                    'preco_desconto_prime': product.preco_desconto_prime,
                    'link_descricao': product.link_descricao,
                    'foto': [foto for foto in product.fotos if foto.principal][0].get_size('m'),
                    'produto_prime': product.produto_prime
                }
                for product in produto.familias[0].produtos if product.codigo != produto.codigo
            ]
        }
        
        response = {
            'brinde': None,
            'oferta': oferta,
            'oferta_inicio': 0,
            'vendedor_nome': vendedor_nome,
            'id_oferta': '',
            'menus': menus,
            'menu': menu_nome,
            'codigo': produto.codigo,
            'nome': produto.nome,
            'familia': familia,
            'fotos': [
                foto.get_size('g') for foto in produto.fotos
            ],
            'disponibilidade': produto.disponibilidade,
            'pre_venda': produto.prevenda,
            'fabricante': {
                'codigo': produto.fabricante.codigo,
                'nome': produto.fabricante.nome,
                'img': produto.fabricante.img
            },
            'preco': produto.preco,
            'preco_prime': produto.preco_prime,
            'preco_desconto': produto.preco_desconto,
            'preco_desconto_prime': produto.preco_desconto_prime,
            'preco_antigo': produto.preco_antigo,
            'economize_prime': produto.economize_prime,
            'descricao': produto.descricao,
            'tag_title': produto.tag_title,
            'tem_frete_gratis': produto.tem_frete_gratis,
            'frete_gratis_somente_prime': produto.frete_gratis_somente_prime,
            'tag_description': produto.tag_description,
            'avaliacao_numero': produto.avaliacao_numero,
            'avaliacao_nota': produto.avaliacao_nota,
            'desconto': produto.desconto,
            'is_openbox': produto.is_openbox,
            'produto_html': produto.produto_html,
            'dimensao_peso': produto.dimensao_peso,
            'peso': produto.peso,
            'garantia': produto.garantia,
            'codigo_anatel': produto.codigo_anatel,
            'produto_especie': produto.produto_especie,
            'link_descricao': produto.link_descricao,
            'origem': produto.origem,
            'origem_nome': produto.origem.nome if produto.origem else None,
            'flag_blackfriday': produto.flag_blackfriday,
            'sucesso': produto.sucesso
        }

        if nova_descricao := produto.nova_descricao:
            response.update({
                'nova_descricao': nova_descricao
            })
        
        return jsonify(response)
        

api.add_resource(ProductDescriptionAPI, '/description/<int:id>', endpoint='products')

